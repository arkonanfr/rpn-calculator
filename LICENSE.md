# LICENSE.md 📜️
  
## © Le Tyrant Louis, décembre 2023.

Ce script peut être distribué librement, selon les termes de la version **_4.0 de la licence Creative Commons Attribution-ShareAlike_**: 

https://creativecommons.org/licenses/by-nc-sa/4.0/ 


<br>

### Vous êtes libre de :

* Partager — copier et redistribuer le script sur n'importe quel support ou format
* S'adapter : remixer, transformer et construire sur le script
* Le donneur de licence ne peut pas révoquer ces libertés tant que vous respectez les termes de la licence.

### Selon les termes suivants :

- Attribution : vous devez attribuer un crédit approprié, fournir un lien vers la licence et indiquer si des modifications ont été apportées. Vous pouvez le faire de toute manière raisonnable, mais pas d'une manière qui suggère que le concédant de licence vous approuve ou votre utilisation.

- Non commercial — Vous ne pouvez pas utiliser le script à des fins commerciales.

- ShareAlike — Si vous remixez, transformez ou construisez sur le script, vous devez distribuer vos contributions sous la même licence que l'original.

- Aucune restriction supplémentaire — Vous ne pouvez pas appliquer de termes juridiques ou de mesures technologiques qui empêchent légalement d'autres personnes de faire ce que la licence permet.
