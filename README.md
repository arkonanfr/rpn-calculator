## Calculatrice RPN 👨‍💻️

Merci d'avoir téléchargé la calculette "rpn.sh".

C'est une calculette RPN écrite en BASH par **_Louis Le Tyrant_**.
Si vous ne savez pas ce qu'est une calculatrice RPN :

https://www.culte-du-code.fr/chapitre/une-calculatrice-rpn-cest-quoi/


## Pour commencer 🏎️

Avoir une machine avec un **système GNU/Linux** d'installé.
<br>

### Pré-requis

Il n'y a aucun pré-requis à avoir. À pars un ordinateur fonctionnel. :)
<br>

## Démarrage

Pour démarrer la calculatrice RPN, il est nécessaire de **clôner le dépôt** puis de rendre le fichier **"rpn.sh" exécutable**.

Pour rendre la calculette exécutable: **_sudo chmod +x rpn.sh_**

Enfin, pour lancer le programme : **_ bash rpn.sh_**    ou     **_./rpn.sh_**
<br>

## Commandes de la calculatrice

-   **help** - Affiche l'aide du programme.
-   **dump** - Affiche le contenu de la stack.
-   **drop** - Efface le contenu de la stack.
-   **exit ou quit** - Quitte le programme.
-   **swap** - Intervertit les deux derniers éléments de la stack.
-   **dup** - Duplique la stack.
-   **clear** - Réinitialise l'affichage de la calculette.
-   **+, add** - Additionne les deux derniers éléments de la stack.
-   **-, sub** - Soustrait l'avant dernier élément de la stack par le dernier.
-   **/, div** - Divise l'avant dernier éléments de la stack par le dernier.
-   ***, mul** - Multiplie les deux derniers éléments de la stack entre eux.
-   **^, puiss** - Permet d'effectuer un calcul de puissance, le 1er nombre est le chiffre principal et le deuxième est l'exposant.
-   **sum** - Additionne tous les éléments de la stack.
<br>

## Scrit codé avec :

[bash](https://www.gnu.org/software/bash/manual/bash.html) - Bourne-Again SHell
<br>

## Versions
**Dernière version :** 1.0
<br>

## Auteurs
**Le Tyrant Louis** _alias_ [arkonanfr](https://gitlab.com/arkonanfr)
<br>

## License

Ce projet est sous licence **Creative Commons Attribution-ShareAlike_4.0** - voir le fichier [LICENSE.md](./LICENSE.md) pour plus d'informations



