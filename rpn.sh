#!/bin/bash


##################################################################
# Initialisation de la calculatrice RPN.

# Initialisation des formats de couleurs.
NO_FORMAT="\033[0m"
C_DARKCYAN="\033[38;5;36m"
C_LIGHTCORAL="\033[38;5;210m"
C_LIGHTGOLDENROD2="\033[38;5;221m"
F_BOLD="\033[1m"
C_INDIANRED1="\033[38;5;203m"
F_UNDERLINED="\033[4m"
F_INVERT="\033[7m"
C_CHARTREUSE2="\033[38;5;82m"

##################################################################
# Interface de Bienvenue.
##################################################################
echo -e "${F_BOLD}Bienvenue dans votre calculatrice 'rpn.sh'${NO_FORMAT} !"
echo -e "${F_BOLD}Merci de lire le ${C_LIGHTGOLDENROD2}README.MD${NO_FORMAT}${F_BOLD} ou de faire la commande ${C_LIGHTGOLDENROD2}'help'${NO_FORMAT}${F_BOLD}.${NO_FORMAT}"

printf " _____________________\n"
printf "|  _________________  |\n"
printf "| | RPN CALCULATOR. | |\n"
printf "| |                 | |\n"
printf "| |    Made         | |\n"
printf "| |    By           | |\n"
printf "| |    Louis        | |\n"
printf "| |    Le Tyrant.   | |\n"
printf "| |_________________| |\n"
printf "|  ___ ___ ___   ___  |\n"
printf "| | 7 | 8 | 9 | | + | |\n"
printf "| |___|___|___| |___| |\n"
printf "| | 4 | 5 | 6 | | - | |\n"
printf "| |___|___|___| |___| |\n"
printf "| | 1 | 2 | 3 | | x | |\n"
printf "| |___|___|___| |___| |\n"
printf "| | . | 0 | = | | / | |\n"
printf "| |___|___|___| |___| |\n"
printf "|_____________________|\n"
printf "\n"
##################################################################

# Initialisation de la stack.
stack=()

# Fonction permettant l'ajout d'un numéro dans la stack.
add_num() {
        stack+=("$1")
}

##################################################################
#Déclaration des modules opératoires
##################################################################

# Pour additionner:

add(){

# Vérification de la stack afin qu'il y ait au moins deux nombres.
    if [ ${#stack[@]} -ge 2 ]; then

        num1=${stack[-1]}
        num2=${stack[-2]}
        stack=("${stack[@]::${#stack[@]}-2}")

        # Fonction permettant d'effectuer une addition.
        result=$(bc <<< "$num1 + $num2")

        # On pousse le résultat dans la stack
        stack+=("$result")

    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
}

# Pour soustraire:

sub() {
    if [ ${#stack[@]} -ge 2 ]; then
        num1=${stack[-1]}
        num2=${stack[-2]}
        stack=("${stack[@]::${#stack[@]}-2}")

        # Fonction permettant d'effectuer une soustraction.
        result=$(bc <<< "$num1 - $num2")
    
        # On pousse le résultat dans la stack
        stack+=("$result")
    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
}


# Pour diviser:

div() {

    if [ ${#stack[@]} -ge 2 ]; then
        num1=${stack[-1]}
        num2=${stack[-2]}
        stack=("${stack[@]::${#stack[@]}-2}")

        if [ "$num1" == "0" ]; then
            echo -e "Erreur. Il est impossible de diviser par zéro."
            return
        fi
        
        # Fonction permettant d'effectuer une division.
        result=$(bc <<< "$num1 / $num2")

        # On pousse le résultat dans la stack
        stack+=("$result")
    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
}



# Pour multiplier:

mul() {

    if [ ${#stack[@]} -ge 2 ]; then
        num1=${stack[-1]}
        num2=${stack[-2]}
        stack=("${stack[@]::${#stack[@]}-2}")

        # Fonction permettant d'effectuer une multiplication.

        result=$(bc <<< "$num1 * $num2")
    
        # On pousse le résultat dans la stack
        stack+=("$result")

    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
}



# Pour faire des calculs de puissances:

puissance() {

    if [ ${#stack[@]} -ge 2 ]; then
        num1=${stack[-1]}
        num2=${stack[-2]}
        stack=("${stack[@]::${#stack[@]}-2}")

        # Fonction permettant d'effectuer un calcul de puissance.
        result=$(bc <<< "$num1 ^ $num2")

        # On pousse le résultat dans la stack
        stack+=("$result")
    
    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
        
}


# Fonction permettant additionner tous les éléments de la stack.
sum() {

    if [ ${#stack[@]} -gt 2 ]; then
        local total=0
        for i in "${stack[@]}"; do
            total=$(bc <<< "$total + $i")
        done
        result="$total"

        #On pousse le résultat dans la stack
        stack+=("$result")
    else
        echo -e "${F_BOLD}${C_INDIANRED1}Vous devez avoir au moins deux nombre dans votre stack afin de pouvoir les calculer.${NO_FORMAT}"
    fi
}


#Déclaration des commandes annexes.
dump() {

    if [ ${#stack[@]} -eq 0 ]; then
        echo "La Stack est vide."
    else
        echo "|=== Stack ===|"
        for i in "${stack[@]}"; do
            echo -e "$i"
        done
        echo "|=============|"
    fi
}

#Supprime le dernier élément de la stack.
drop() {

    unset "stack[-1]"
    stack=("${stack[@]}")

}

swap() {

    if [ ${#stack[@]} -ge 2 ]; then
        echo -e "${F_BOLD}${C_INDIANRED1}Impossible d'échanger la stack - Le nombre d'éléments est insuffisant.${NO_FORMAT}"
        return
    fi
        local last_index=$((${#stack[@]}-1))
        local second_to_last_index=$((${#stack[@]}-2))
        local temp="${stack[last_index]}"
        stack[last_index]="${stack[second_to_last_index]}"
        stack[second_to_last_index]="$temp"
}


dup() {
    
    if [ ${#stack[@]} -lt 1 ]; then
        echo -e "${F_BOLD}${C_INDIANRED1}Impossible de dupliquer la stack - La stack est vide.${NO_FORMAT}"
       return
    fi
        top=${stack[${#stack[@]}-1]}

        #On pousse le résultat dans la stack
        stack+=("$top")
}



# Variable permettant d'épurer l'affichage
effacer_aff() {
    clear
}



while true; do

    echo -n "[${#stack[@]}] - Entrer votre commande:"
    read -r command

    #Vérification de la réponse donnée
    nombre='^[0-9]+([.][0-9]+)?$'
    if [[ $command =~ $nombre ]]; then
        add_num "$command"
    else
        case $command in
            #Liste des commandes de la Calcultrice RPN:
            "help"|"h")
                echo -e "${C_DARKCYAN}help - Affiche l'aide du programme."
                echo -e "${C_DARKCYAN}dump - Affiche le contenu de la stack."
                echo -e "${C_DARKCYAN}drop - Efface le contenu de la stack."
                echo -e "${C_DARKCYAN}exit ou quit - Quitte le programme."
                echo -e "${C_DARKCYAN}swap - Intervertit les deux derniers éléments de la stack."
                echo -e "${C_DARKCYAN}dup - Duplique la stack."
                echo -e "${C_DARKCYAN}clear - Réinitialise l'affichage de la calculette"
                echo -e "${NO_FORMAT}-------------------------------------------------------------"
                echo -e "${C_LIGHTCORAL}+, add - Additionne les deux derniers éléments de la stack"
                echo -e "${C_LIGHTCORAL}-, sub - Soustrait l'avant dernier élément de la stack par le dernier"
                echo -e "${C_LIGHTCORAL}/, div - Divise l'avant dernier éléments de la stack par le dernier"
                echo -e "${C_LIGHTCORAL}*, mul - Multiplie les deux derniers éléments de la stack entre eux"
                echo -e "${C_LIGHTCORAL}^, puiss - Permet d'effectuer un calcul de puissance, le 1er nombre est le chiffre principal et le deuxième est l'exposant."
                echo -e "${C_LIGHTCORAL}sum - Additionne tous les éléments de la stack et pousse le résultat sur la stack.${NO_FORMAT}"
                ;;

            dump)
                dump                
                ;;

            drop)
                drop
                ;;

            exit|quit)
                echo -e "${F_UNDERLINED}${F_INVERT}${F_BOLD}${C_CHARTREUSE2}La calculette va s'arrêter.${NO_FORMAT}"
                break
                ;;

            swap)
                swap
                ;;

            dup)
                dup
                ;;
            
            clear)
                effacer_aff
                ;;

            "+"|add)
                add
                ;;

            "-"|sub)
                sub
                ;;

            "/"|div)
                div
                ;;

            "*"|mul)
                mul
                ;;

            "^"|puiss)
                puissance
                ;;

            sum)
                sum
                ;;

            *)
                echo "Opération ou commande inconnue."
                ;;
        esac
    fi
done